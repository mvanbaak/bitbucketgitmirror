#!/usr/bin/env python

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import simplejson
import cgi
import subprocess

PORT_NUMBER = 8080
BASE_DIR = '/home/git/Qelp/'

# Class to handle the incoming requests
class MyRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        return
    def do_POST(self):
        formdata = cgi.FieldStorage(
            fp = self.rfile,
            headers = self.headers,
            environ = {
                'REQUEST_METHOD':'POST',
                'CONTENT_TYPE':self.headers['Content-Type'],
            }
        )
        self.send_response(200)
        self.end_headers()

        if 'payload' in formdata.keys():
            payload = formdata['payload'].value
            data = simplejson.loads(payload)
            if 'repository' in data.keys():
                if 'slug' in data['repository'].keys():
                    repo_dir = "%s%s.git" % (BASE_DIR, data['repository']['slug'])

                    print "We are going to update mirror of repo '%s'" % repo_dir

                    exitcode = -127
                    stdout = ""
                    stderr = ""
                    p = subprocess.Popen(
                        ['git', 'remote', 'update'],
                        cwd=repo_dir,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                    )
                    stdout, stderr = p.communicate()
                    exitcode = p.wait()

                    print "Done with exitcode '%s'\n" % exitcode

try:
    #Create a web server and define the handler to manage the incoming request
    server = HTTPServer(('', PORT_NUMBER), MyRequestHandler)
    print 'Started httpserver on port ' , PORT_NUMBER

    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
